package org.rscdaemon.server.event;

import org.rscdaemon.server.model.Mob;
import org.rscdaemon.server.model.npc.Npc;
import org.rscdaemon.server.model.player.Player;

public abstract class WalkToPlayerEvent extends DelayedEvent {
	protected Mob affectedMob;
	private int radius;
	
	public WalkToPlayerEvent(Npc owner, Player affectedMob, int radius) {
		super(affectedMob, 500);
		this.affectedMob = affectedMob;
		this.radius = radius;
		if(owner.withinRange(affectedMob.getLocation(), radius)) {
			arrived();
			super.running = false;
		}
	}
	
	public final void run() {
		if(owner.withinRange(affectedMob.getLocation(), radius)) {
			arrived();
		}
		else if(owner.hasMoved()) {
			return; // We're still moving
		}
		else {
			failed();
		}
		super.running = false;
	}
	
	public abstract void arrived();
	
	public void failed() { } // Not abstract as isn't required
	
	public Mob getAffectedMob() {
		return affectedMob;
	}

}