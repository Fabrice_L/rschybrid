package org.rscdaemon.server.event;

import org.rscdaemon.server.GameVars;
import org.rscdaemon.server.model.Mob;
import org.rscdaemon.server.model.Projectile;
import org.rscdaemon.server.model.item.InvItem;
import org.rscdaemon.server.model.item.Item;
import org.rscdaemon.server.model.landscape.ProjectilePath;
import org.rscdaemon.server.model.npc.Npc;
import org.rscdaemon.server.model.player.Player;
import org.rscdaemon.server.states.Action;
import org.rscdaemon.server.util.DataConversions;
import org.rscdaemon.server.util.Formulae;

import java.util.ArrayList;

public class RangeEvent extends DelayedEvent {
	private Mob affectedMob;
	private boolean firstRun = true;
	
	public RangeEvent(Player owner, Mob affectedMob) {
		super(owner, GameVars.rangedDelaySpeed);
		this.affectedMob = affectedMob;
	}
	
	private Item getArrows(int id) {
		for(Item i : world.getTile(affectedMob.getLocation()).getItems()) {
			if(i.getID() == id && i.visibleTo(owner) && !i.isRemoved()) {
				return i;
			}
		}
		return null;
	}
	
	public void run() {
		int bowID = owner.getRangeEquip();
		if(!owner.loggedIn() || (affectedMob instanceof Player && !((Player)affectedMob).loggedIn()) || affectedMob.getHits() <= 0 || !owner.checkAttack(affectedMob, true) || bowID < 0) {
			owner.resetRange();
			return;
		}
		if(owner.withinRange(affectedMob, 5)) {
			if(owner.isFollowing()) {
				owner.resetFollowing();
			}
			if(!owner.finishedPath()) {
				owner.resetPath();
			}
		}
		else {
			owner.setFollowing(affectedMob);
			return;
		}
		ProjectilePath path = new ProjectilePath(owner.getX(), owner.getY(),
				affectedMob.getX(), affectedMob.getY());
		if (!path.isValid()) {
			owner.getActionSender().sendMessage(
					"I can't get a clear shot from here");
			owner.resetPath();
			owner.resetRange();
			this.stop();
			return;
		}
		boolean xbow = DataConversions.inArray(Formulae.xbowIDs, bowID);
		int arrowID = -1;
		for(int aID : (xbow ? Formulae.boltIDs : Formulae.arrowIDs)) {
			int slot = owner.getInventory().getLastIndexById(aID);
			if(slot < 0) {
				continue;
			}
			InvItem arrow = owner.getInventory().get(slot);
			if(arrow == null) { // This shouldn't happen
				continue;
			}
			arrowID = aID;
			int newAmount = arrow.getAmount() - 1;
			if(newAmount <= 0) {
				owner.getInventory().remove(slot);
				owner.getActionSender().sendInventory();
			}
			else {
				arrow.setAmount(newAmount);
				owner.getActionSender().sendUpdateItem(slot);
			}
			break;
		}
		if(arrowID < 0) {
			owner.getActionSender().sendMessage("You have run out of " + (xbow ? "bolts" : "arrows"));
			owner.resetRange();
			return;
		}
		if(affectedMob.isPrayerActivated(13)) {
			owner.getActionSender().sendMessage("Your missiles are blocked");
			owner.resetRange();
			return;
		}
		int damage = Formulae.calcRangeHit(owner.getCurStat(4), owner.getRangePoints(), affectedMob.getArmourPoints(), arrowID);
		if(!Formulae.looseArrow(damage)) {
			Item arrows = getArrows(arrowID);
			if(arrows == null) {
				world.registerItem(new Item(arrowID, affectedMob.getX(), affectedMob.getY(), 1, owner));
			}
			else {
				arrows.setAmount(arrows.getAmount() + 1);
			}
		}
		if(firstRun) {
			firstRun = false;
			if(affectedMob instanceof Player) {
				((Player)affectedMob).getActionSender().sendMessage(owner.getUsername() + " is shooting at you!");
			}
		}
		Projectile projectile = new Projectile(owner, affectedMob, 2);
  		affectedMob.setLastDamage(damage);
  		int newHp = affectedMob.getHits() - damage;
  		affectedMob.setHits(newHp);
  		ArrayList<Player> playersToInform = new ArrayList<Player>();
  		playersToInform.addAll(owner.getViewArea().getPlayersInView());
  		playersToInform.addAll(affectedMob.getViewArea().getPlayersInView());
  		for(Player p : playersToInform) {
  			p.informOfProjectile(projectile);
  			p.informOfModifiedHits(affectedMob);
  		}
  		if(affectedMob instanceof Player) {
  			Player affectedPlayer = (Player)affectedMob;
  			affectedPlayer.getActionSender().sendStat(3);
		}
		owner.getActionSender().sendSound("shoot");
		owner.setArrowFired();
  		if(newHp <= 0) {
  			affectedMob.killedBy(owner, false);
  			int exp = Formulae.combatExperience(affectedMob);
  			owner.incExp(4, exp, true, true);
  			owner.getActionSender().sendStat(4);
  			owner.resetRange();
  		} else {
			if (owner instanceof Player && affectedMob instanceof Npc) {
				final Npc npc = (Npc) affectedMob;
				final Player player = (Player) owner;
				
				if (npc.isBusy() || npc.getChasing() != null)
					return;
				
				npc.resetPath();
				npc.setChasing(player);
				
				// Radius is 0 to prevent wallhacking by NPCs. Easiest
				// method I
				// can come up with for now.
				world.getDelayedEventHandler().add(
						new WalkMobToMobEvent(affectedMob, owner, 0) {
								public void arrived() {
									if (affectedMob.isBusy() || player.isBusy()) {
										npc.setChasing(null);
										this.stop();
										return;
									}
				
									npc.resetPath();
									player.setBusy(true);
									player.resetPath();
									player.resetAll();
				
									player.setStatus(Action.FIGHTING_MOB);
									player.getActionSender().sendSound("underattack");
									player.getActionSender().sendMessage("You are under attack!");
				
									npc.setLocation(player.getLocation(), true);
									for (Player p : npc.getViewArea().getPlayersInView())
										p.removeWatchedNpc(npc);
				
									player.setBusy(true);
									player.setSprite(9);
									player.setOpponent(npc);
									player.setCombatTimer();
				
									npc.setBusy(true);
									npc.setSprite(8);
									npc.setOpponent(player);
									npc.setCombatTimer();
				
									npc.setChasing(null);
				
									FightEvent fighting = new FightEvent(player, npc, true);
									fighting.setLastRun(0);
									world.getDelayedEventHandler().add(fighting);
									this.stop();
								}
				
								public void failed() {
									npc.setChasing(null);
								}
						});
						// target is still alive? is still around?
					if (!npc.isRemoved() || owner.withinRange(npc)) {
						return;
					}
					this.stop();
				}
			}  			
	}
	
	public Mob getAffectedMob() {
		return affectedMob;
	}
	
	public boolean equals(Object o) {
		if(o instanceof RangeEvent) {
			RangeEvent e = (RangeEvent)o;
			return e.belongsTo(owner);
		}
		return false;
	}
}