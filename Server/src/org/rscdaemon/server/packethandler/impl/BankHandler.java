package org.rscdaemon.server.packethandler.impl;

import org.rscdaemon.server.packethandler.PacketHandler;
import org.rscdaemon.server.entityhandling.EntityHandler;
import org.rscdaemon.server.model.*;
import org.rscdaemon.server.model.item.Bank;
import org.rscdaemon.server.model.item.Certificates;
import org.rscdaemon.server.model.item.InvItem;
import org.rscdaemon.server.model.item.Inventory;
import org.rscdaemon.server.model.player.Player;
import org.rscdaemon.server.net.Packet;
import org.rscdaemon.server.net.RSCPacket;
import org.apache.mina.common.IoSession;

public class BankHandler implements PacketHandler {
	/**
	 * World instance
	 */
	public static final World world = World.getWorld();

	public void handlePacket(Packet p, IoSession session) throws Exception {
		Player player = (Player)session.getAttachment();
		int pID = ((RSCPacket)p).getID();
		if(player.isBusy() || player.isRanging() || player.isTrading() || player.isDueling()) {
			player.resetBank();
			return;
		}
  		if(!player.accessingBank()) {
  			player.setSuspiciousPlayer(true);
  			player.resetBank();
  			return;
  		}
  		Bank bank = player.getBank();
  		Inventory inventory = player.getInventory();
  		InvItem item;
  		int itemID, amount, slot;
		switch(pID) {
			case 48: // Close bank
				player.resetBank();
				break;
			case 198: // Deposit item
				itemID = p.readShort();
				amount = p.readInt();
				if(amount < 1) {
		      			player.setSuspiciousPlayer(true);
		      			return;
				}
				
				if (inventory.countId(itemID) < amount) {
					amount = inventory.countId(itemID);
				}
				
				int uncertedId = Certificates.getItemId(itemID);
				if (uncertedId == -1) {
					uncertedId = itemID;
				}
				
				if(EntityHandler.getItemDef(itemID).isStackable()) {
					item = new InvItem(uncertedId, amount);
					if(bank.canHold(item) && inventory.remove(new InvItem(itemID, amount) ) > -1) {
						bank.add(item);
					}
					else {
						player.getActionSender().sendMessage("You don't have room for that in your bank");
					}
				}
				else {
					for(int i = 0;i < amount;i++) {
						int idx = inventory.getLastIndexById(itemID);
						item = inventory.get(idx);
						if(item == null) { // This shouldn't happen
							break;
						}
						if(bank.canHold(item) && inventory.remove(item) > -1) {
							bank.add(item);
						}
						else {
							player.getActionSender().sendMessage("You don't have room for that in your bank");
							break;
						}
					}
				}
				slot = bank.getFirstIndexById(uncertedId);
				if(slot > -1) {
					player.getActionSender().sendInventory();
					player.getActionSender().updateBankItem(slot, uncertedId, bank.countId(uncertedId));
				}
				break;
			case 183: // Withdraw item
				itemID = p.readShort();
				amount = p.readInt();
				if(amount < 1 || bank.countId(itemID) < amount) {
		      			player.setSuspiciousPlayer(true);
		      			return;
				}
				
				int certedId = Certificates.getCertId(itemID);
				if (player.getGameSetting(7)) {
					if (certedId == -1) {
						certedId = itemID;
						player.getActionSender().sendMessage("This item does not have a certificate.");
					}
				} else {
					certedId = itemID;
				}
				
				slot = bank.getFirstIndexById(itemID);
				if(EntityHandler.getItemDef(certedId).isStackable()) {
					item = new InvItem(certedId, amount);
					if(inventory.canHold(item) && bank.remove(new InvItem(itemID, amount)) > -1) {
						inventory.add(item);
					}
					else {
						player.getActionSender().sendMessage("You don't have room for that in your inventory");
					}
				}
				else {
					for(int i = 0;i < amount;i++) {
						if(bank.getFirstIndexById(itemID) < 0) { // This shouldn't happen
							break;
						}
						item = new InvItem(certedId, 1);
						if(inventory.canHold(item) && bank.remove(new InvItem(itemID, 1)) > -1) {
							inventory.add(item);
						}
						else {
							player.getActionSender().sendMessage("You don't have room for that in your inventory");
							break;
						}
					}
				}
				if(slot > -1) {
					player.getActionSender().sendInventory();
					player.getActionSender().updateBankItem(slot, itemID, bank.countId(itemID));
				}
				break;
		}
	}
	
}