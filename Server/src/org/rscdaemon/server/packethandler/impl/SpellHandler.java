package org.rscdaemon.server.packethandler.impl;

import org.rscdaemon.server.packethandler.PacketHandler;
import org.rscdaemon.server.model.*;
import org.rscdaemon.server.model.item.InvItem;
import org.rscdaemon.server.model.item.Item;
import org.rscdaemon.server.model.landscape.ActiveTile;
import org.rscdaemon.server.model.npc.Npc;
import org.rscdaemon.server.model.player.Player;
import org.rscdaemon.server.net.Packet;
import org.rscdaemon.server.net.RSCPacket;
import org.rscdaemon.server.util.*;
import org.rscdaemon.server.event.WalkToMobEvent;
import org.rscdaemon.server.event.WalkToPointEvent;
import org.rscdaemon.server.states.Action;
import org.rscdaemon.server.entityhandling.EntityHandler;
import org.rscdaemon.server.entityhandling.defs.SpellDef;
import org.rscdaemon.server.entityhandling.defs.extras.ItemSmeltingDef;
import org.rscdaemon.server.entityhandling.defs.extras.ReqOreDef;
import org.rscdaemon.server.event.ObjectRemover;

import org.apache.mina.common.IoSession;

import java.util.*;
import java.util.Map.Entry;

public class SpellHandler implements PacketHandler {
	/**
	 * World instance
	 */
	public static final World world = World.getWorld();
	private Random r = new Random();

	public final int Rand(int low, int high)
	{
       	return low + r.nextInt(high - low);
    }

	public void handlePacket(Packet p, IoSession session) throws Exception {
		Player player = (Player)session.getAttachment();
		int pID = ((RSCPacket)p).getID();
		if((player.isBusy() && !player.inCombat()) || player.isRanging()) {
			return;
		}
		if(player.isDueling() && player.getDuelSetting(1)) {
			player.getActionSender().sendMessage("Magic is disabled in this duel");
			return;
		}
		player.resetAllExceptDueling();
		int idx = p.readShort();
		if(idx < 0 || idx >= 49) {
			player.setSuspiciousPlayer(true);
			return;
		}
		if(!canCast(player)) {
			return;
		}
		SpellDef spell = EntityHandler.getSpellDef(idx);
		if(player.getCurStat(6) < spell.getReqLevel()) {
			player.setSuspiciousPlayer(true);
			player.getActionSender().sendMessage("Your magic ability is not high enough for this spell.");
			player.resetPath();
			return;
		}
		if (!spell.getName().toLowerCase().contains("teleport") && pID != 206) {
			if(!Formulae.castSpell(spell, player.getCurStat(6), player.getMagicPoints())) {
				player.getActionSender().sendMessage("The spell fails, you may try again in 5 seconds.");
				player.getActionSender().sendSound("spellfail");
				player.setSpellFail();
				player.resetPath();
				return;
			}
		}
		switch(pID) {
			case 206: // Cast on self
				if(player.isDueling()) {
					player.getActionSender().sendMessage("This type of spell cannot be used in a duel.");
					return;
				}
				if(spell.getSpellType() == 0) {
					handleTeleport(player, spell, idx);
				}
				//if(spell.getSpellType() == 6) {
					handleGroundCast(player, spell, idx);
				//}
				break;
			case 55: // Cast on player
				if(spell.getSpellType() == 1 || spell.getSpellType() == 2) {
					Player affectedPlayer = world.getPlayer(p.readShort());
					if(affectedPlayer == null) { // This shouldn't happen
						player.resetPath();
						return;
					}
					if(player.withinRange(affectedPlayer, 5)) {
						player.resetPath();
					}
					handleMobCast(player, affectedPlayer, idx);
				}
			//					if(spell.getSpellType() == 6) {
			//						handleGroundCast(player, spell);
			//	}
				break;
			case 71: // Cast on npc
				if(spell.getSpellType() == 2) {
					Npc affectedNpc = world.getNpc(p.readShort());
					if(affectedNpc == null) { // This shouldn't happen
						player.resetPath();
						return;
					}
					if(player.withinRange(affectedNpc, 5)) {
						player.resetPath();
					}
					handleMobCast(player, affectedNpc, idx);
				}
			//					if(spell.getSpellType() == 6) {
			//						handleGroundCast(player, spell);
			//	}
				break;
			case 49: // Cast on inventory item
				if(player.isDueling()) {
					player.getActionSender().sendMessage("This type of spell cannot be used in a duel.");
					return;
				}
				if(spell.getSpellType() == 3) {
					InvItem item = player.getInventory().get(p.readShort());
					if(item == null) { // This shoudln't happen
						player.resetPath();
						return;
					}
					handleInvItemCast(player, spell, idx, item);
				}
			//					if(spell.getSpellType() == 6) {
			//						handleGroundCast(player, spell);
			//	}
				break;
			case 67: // Cast on door - type 4
				if(player.isDueling()) {
					player.getActionSender().sendMessage("This type of spell cannot be used in a duel.");
					return;
				}
				player.getActionSender().sendMessage("@or1@This type of spell is not yet implemented.");
			//					if(spell.getSpellType() == 6) {
			//						handleGroundCast(player, spell);
			//	}
				break;
			case 17: // Cast on game object - type 5
				if(player.isDueling()) {
					player.getActionSender().sendMessage("This type of spell cannot be used in a duel.");
					return;
				}
				player.getActionSender().sendMessage("@or1@This type of spell is not yet implemented.");
						//		if(spell.getSpellType() == 6) {
						//			handleGroundCast(player, spell);
			//	}
				break;
			case 104: // Cast on ground item
				if(player.isDueling()) {
					player.getActionSender().sendMessage("This type of spell cannot be used in a duel.");
					return;
				}
				ActiveTile t = world.getTile(p.readShort(), p.readShort());
				int itemId = p.readShort();
				Item affectedItem = null;
				for(Item i : t.getItems()) {
					if(i.getID() == itemId) {
						affectedItem = i;
						break;
					}
				}
				if(affectedItem == null) { // This shouldn't happen
					return;
				}
				handleItemCast(player, spell, idx, affectedItem);
				break;
			case 232: // Cast on ground - type 6
				if(player.isDueling()) {
					player.getActionSender().sendMessage("This type of spell cannot be used in a duel.");
					return;
				}
				//if(spell.getSpellType() == 6) {
					handleGroundCast(player, spell, idx);
				//}
				break;
		}
		player.getActionSender().sendInventory();
		player.getActionSender().sendStat(6);
	}

	private void handleMobCast(Player player, Mob affectedMob, final int spellID) {
		if(!player.isBusy()) {
			player.setFollowing(affectedMob);
		}
		player.setStatus(Action.CASTING_MOB);
		world.getDelayedEventHandler().add(new WalkToMobEvent(player, affectedMob, 5) {
			public void arrived() {
				owner.resetPath();
				SpellDef spell = EntityHandler.getSpellDef(spellID);
				if(!canCast(owner) || affectedMob.getHits() <= 0 || !owner.checkAttack(affectedMob, true) || owner.getStatus() != Action.CASTING_MOB) {
					return;
				}
				owner.resetAllExceptDueling();
				switch(spellID) {


					default:
						if(!checkAndRemoveRunes(owner, spell)) {
							return;
						}
						if(affectedMob instanceof Player && !owner.isDueling()) {
							Player affectedPlayer = (Player)affectedMob;
							owner.setSkulledOn(affectedPlayer);
				      		}
						int damage = Formulae.calcSpellHit(EntityHandler.getSpellAggressiveLvl(spellID), owner.getMagicPoints());
						if(affectedMob instanceof Player) {
							Player affectedPlayer = (Player)affectedMob;
							affectedPlayer.getActionSender().sendMessage(owner.getUsername() + " is shooting at you!");
						}
				      		Projectile projectile = new Projectile(owner, affectedMob, 1);
				      		affectedMob.setLastDamage(damage);
				      		int newHp = affectedMob.getHits() - damage;
				      		affectedMob.setHits(newHp);
				      		ArrayList<Player> playersToInform = new ArrayList<Player>();
				      		playersToInform.addAll(owner.getViewArea().getPlayersInView());
				      		playersToInform.addAll(affectedMob.getViewArea().getPlayersInView());
				      		for(Player p : playersToInform) {
				      			p.informOfProjectile(projectile);
				      			p.informOfModifiedHits(affectedMob);
				      		}
				      		if(affectedMob instanceof Player) {
				      			Player affectedPlayer = (Player)affectedMob;
							affectedPlayer.getActionSender().sendStat(3);
						}
				      		if(newHp <= 0) {
				      			affectedMob.killedBy(owner, owner.isDueling());
				      		}
				      		finalizeSpell(owner, spell);
						break;
				}
				owner.getActionSender().sendInventory();
				owner.getActionSender().sendStat(6);
			}
		});
	}

		public void godSpellObject(Mob affectedMob, int spell)
		{
			switch(spell)
			{

			}
	}

	private void handleItemCast(Player player, final SpellDef spell, final int id, final Item affectedItem) {
		player.setStatus(Action.CASTING_GITEM);
		world.getDelayedEventHandler().add(new WalkToPointEvent(player, affectedItem.getLocation(), 5, true) {
			public void arrived() {
				owner.resetPath();
				ActiveTile tile = world.getTile(location);
				if(!canCast(owner) || !tile.hasItem(affectedItem) || owner.getStatus() != Action.CASTING_GITEM) {
					return;
				}
				owner.resetAllExceptDueling();
				switch(id) {

				}
				owner.getActionSender().sendInventory();
				owner.getActionSender().sendStat(6);
			}
		});
	}

	private void handleInvItemCast(Player player, SpellDef spell, int id, InvItem affectedItem) {
		switch(id) {

			case 11: // Low level alchemy
				if(affectedItem.getID() == 10) {
					player.getActionSender().sendMessage("You cannot alchemy that");
					return;
				}
				if(!checkAndRemoveRunes(player, spell)) {
					return;
				}
				if(player.getInventory().remove(affectedItem) > 1) {
					int value = (int)(affectedItem.getDef().getBasePrice() * 0.4D * affectedItem.getAmount());
					player.getInventory().add(new InvItem(10, value)); // 40%
				}
				finalizeSpell(player, spell);
				break;
			case 19: // High level alchemy
				if(affectedItem.getID() == 10) {
					player.getActionSender().sendMessage("You cannot alchemy that");
					return;
				}
				if(!checkAndRemoveRunes(player, spell)) {
					return;
				}
				if(player.getInventory().remove(affectedItem) > -1) {
					int value = (int)(affectedItem.getDef().getBasePrice() * 0.6D * affectedItem.getAmount());
					player.getInventory().add(new InvItem(10, value)); // 60%
				}
				finalizeSpell(player, spell);
				break;
		}
		if(affectedItem.isWielded()) {
			player.getActionSender().sendSound("click");
			affectedItem.setWield(false);
			player.updateWornItems(affectedItem.getWieldableDef().getWieldPos(), player.getPlayerAppearance().getSprite(affectedItem.getWieldableDef().getWieldPos()));
			player.getActionSender().sendEquipmentStats();
		}
	}

	private void handleGroundCast(Player player, SpellDef spell, int id) {
		switch(id) {
		}
	}

	private void handleTeleport(Player player, SpellDef spell, int id) {
      		if(player.inCombat()) {
      			player.getActionSender().sendMessage("You cannot teleport while in combat.");
      			return;
      		}
		if(player.getLocation().wildernessLevel() >= 20 || (player.getLocation().inModRoom() && !player.isMod())) {
			player.getActionSender().sendMessage("A magical force stops you from teleporting.");
			return;
		}

		switch(id) {
			case 0: // Yanille
				player.teleport(588, 752, true);
				break;
			case 1: // Seers
				player.teleport(500, 455, true);
				break;
			case 2: // Edgeville
				player.teleport(216, 449, true);
				break;
			case 3: // Catherby
				player.teleport(440, 501, true);
				break;
			case 4: // Legends Guild
				player.teleport(513, 551, true);
				break;
			case 5: // Agility Arena
				player.teleport(695, 492, true);
				break;
		}
		finalizeSpell(player, spell);
	}

	private static boolean canCast(Player player) {
		if(!player.castTimer()) {
			player.getActionSender().sendMessage("You must wait another " + player.getSpellWait() + " seconds to cast another spell.");
			player.resetPath();
			return false;
		}
		return true;
	}

	private static boolean checkAndRemoveRunes(Player player, SpellDef spell) {
		for(Entry<Integer, Integer> e : spell.getRunesRequired()) {
			boolean skipRune = false;
			for(InvItem staff : getStaffs(e.getKey())) {
				if(player.getInventory().contains(staff)) {
					for(InvItem item : player.getInventory().getItems()) {
						if(item.equals(staff) && item.isWielded()) {
							skipRune = true;
							break;
						}
					}
				}
			}
			if(skipRune) {
				continue;
			}
			if(player.getInventory().countId(((Integer)e.getKey()).intValue()) < ((Integer)e.getValue()).intValue()) {
				player.setSuspiciousPlayer(true);
				player.getActionSender().sendMessage("You don't have all the reagents you need for this spell");
				return false;
			}
		}
      		for(Entry<Integer, Integer> e : spell.getRunesRequired()) {
			boolean skipRune = false;
			for(InvItem staff : getStaffs(e.getKey())) {
				if(player.getInventory().contains(staff)) {
					for(InvItem item : player.getInventory().getItems()) {
						if(item.equals(staff) && item.isWielded()) {
							skipRune = true;
							break;
						}
					}
				}
			}
			if(skipRune) {
				continue;
			}
			player.getInventory().remove(((Integer)e.getKey()).intValue(), ((Integer)e.getValue()).intValue());
      		}
		return true;
	}

	private void finalizeSpell(Player player, SpellDef spell) {
		player.getActionSender().sendSound("spellok");
		player.getActionSender().sendMessage("Cast spell successfully");
		player.incExp(6, spell.getExp(), true, true);
		player.setCastTimer();
	}

	private static InvItem[] getStaffs(int runeID) {
		InvItem[] items = staffs.get(runeID);
		if(items == null) {
			return new InvItem[0];
		}
		return items;

	}

	private static TreeMap<Integer, InvItem[]> staffs = new TreeMap<Integer, InvItem[]>();

	static {
		staffs.put(31, new InvItem[]{new InvItem(197), new InvItem(615), new InvItem(682)}); // Fire-Rune
		staffs.put(32, new InvItem[]{new InvItem(102), new InvItem(616), new InvItem(683)}); // Water-Rune
		staffs.put(33, new InvItem[]{new InvItem(101), new InvItem(617), new InvItem(684)}); // Air-Rune
		staffs.put(34, new InvItem[]{new InvItem(103), new InvItem(618), new InvItem(685)}); // Earth-Rune
	}

}
