package org.rscdaemon.server.npchandler;

import org.rscdaemon.server.model.npc.Npc;
import org.rscdaemon.server.model.player.Player;

public interface NpcHandler {
	public void handleNpc(final Npc npc, Player player) throws Exception;
}
