package org.rscdaemon.server.model.landscape;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class TileValue {

	private static final List<TileValue> tiles = new CopyOnWriteArrayList<TileValue>();

	public static TileValue create(int diagWallVal, byte[] v) {
		TileValue curr = new TileValue(diagWallVal, v);
		int index = tiles.indexOf(curr);
		if (index == -1) {
			tiles.add(curr);
			return curr;
		}
		return tiles.get(index);
	}

	private TileValue(int diagWallVal, byte[] v) {
		if (v.length != 6) {
			throw new IndexOutOfBoundsException("Must have a size of 6");
		}

		this.diagWallVal = diagWallVal;
		this.horizontalWallVal = v[0];
		this.mapValue = v[1];
		this.objectValue = v[2];
		this.overlay = v[3];
		this.verticalWallVal = v[4];
		this.elevation = v[5];
	}

	private final int diagWallVal;
	private final byte horizontalWallVal;
	public final byte mapValue;
	public final byte objectValue;
	final byte overlay;
	private final byte verticalWallVal;
	private final byte elevation;

	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof TileValue)) {
			return false;
		}

		TileValue t = (TileValue) o;

		if (this.getDiagWallVal() == t.getDiagWallVal()
				&& this.getHorizontalWallVal() == t.getHorizontalWallVal()
				&& this.mapValue == t.mapValue
				&& this.objectValue == t.objectValue
				&& this.overlay == t.overlay
				&& this.getVerticalWallVal() == t.getVerticalWallVal()
				&& this.getElevation() == t.getElevation()) {
			return true;
		}

		return false;
	}

	public byte getHorizontalWallVal() {
		return horizontalWallVal;
	}

	public byte getVerticalWallVal() {
		return verticalWallVal;
	}

	public int getDiagWallVal() {
		return diagWallVal;
	}

	public byte getElevation() {
		return elevation;
	}
}