package org.rscdaemon.server.model.landscape;

public class MutableTileValue {

	public MutableTileValue(TileValue t) {
		this.diagWallVal = t.getDiagWallVal();
		this.horizontalWallVal = t.getHorizontalWallVal();
		this.mapValue = t.mapValue;
		this.objectValue = t.objectValue;
		this.verticalWallVal = t.getVerticalWallVal();
		this.elevation = t.getElevation();
	}

	public int diagWallVal;
	public byte horizontalWallVal;
	public byte mapValue;
	public byte objectValue;
	public byte overlay;
	public byte verticalWallVal;
	public byte elevation;

	public TileValue toTileValue() {
		return TileValue.create(diagWallVal, new byte[] { horizontalWallVal,
				mapValue, objectValue, overlay, verticalWallVal, elevation });
	}
}