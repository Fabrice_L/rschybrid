package org.rscdaemon.server.model.landscape;

import org.rscdaemon.server.entityhandling.defs.DoorDef;
import org.rscdaemon.server.model.GameObject;
import org.rscdaemon.server.model.World;

public class ProjectilePath {
	int xi;
	int xf;
	int yf;
	int yi;
	//owner.getX(), owner.getY(),affectedMob.getX(), affectedMob.getY()
	public ProjectilePath(int xi, int yi, int xf, int yf) {
		this.xi = xi;
		this.yi = yi;
		this.xf = xf;
		this.yf = yf;
	}		
	
	private int[][] allowedWalltypes = {
			{4, 5, 6, 42, 14}, //Normal
			{229, 5, 6}	//Diagonal
	};
	
	public int getDir(int x, int y) {
		if (Math.abs(x) > Math.abs(y)) {
			return 1; 
		} else if (Math.abs(x) < Math.abs(y)) {
			return 2;
		} else {
			return 3;
		}
	}
	
	public boolean isValid() {
		if (xi == xf && yf == yi) {
			return true;
		}

		int vX = xf - xi;
		int vY = yf - yi;
		int dir = getDir(vX, vY);
		// The minimum step allowed to ensure we don't miss a square
		double tStep = ((dir == 1 || dir == 3) ? 
				Math.abs(0.5 / vX) :// Comes from r(t) = <Vx * t + xi, Vy * t + yi>
				Math.abs(0.5 / vY)); // Comes from r(t) = <Vx * t + xi, Vy * t + yi>
		int prevX = xi;
		int prevY = yi;
		double t = tStep;
		while (t <= 1 + tStep) {
			int currX = (int) (vX * t + xi);
			int currY = (int) (vY * t + yi);
			if (prevX != currX || prevY != currY) {
				boolean cont = true;

				if (prevX > currX) {
					cont &= checkEast(prevX, prevY, dir);
				}
				if (prevX < currX) {
					cont &= checkWest(prevX, prevY, dir);
				}
				if (prevY < currY) {
					cont &= checkSouth(prevX, prevY, dir);
				}
				if (prevY > currY) {
					cont &= checkNorth(prevX, prevY, dir);
				}

				if (!cont) 
					return false;
			}

			if (prevX == xf &&  prevY == yf) 
				return true;
			prevX = currX;
			prevY = currY;
			t += tStep;
		}
		return true;
	}
	
	private boolean checkNorth(int x, int y, int dir) {
		return checkTile(1, x, y, dir);
	}
	
	private boolean checkSouth(int x, int y, int dir) {
		return checkTile(4, x, y, dir);
	}
	
	private boolean checkEast(int x, int y, int dir) {
		return checkTile(2, x, y, dir);
	}
	
	private boolean checkWest(int x, int y, int dir) {
		return checkTile(8, x, y, dir);
	}
	
	private boolean containsWallType(int index, int j) {
		for (int i : allowedWalltypes[index]) {
			if (i == j) {
				return true;
			}
		}
		return false;
	}
	
	private boolean checkTile(int face, int x, int y, int dir) {
		TileValue t = World.getWorld().getTileValue(x, y);
		int b = t.mapValue;
		if ((face & b) != 0) {
			if ((dir == 1 || dir == 3) && !containsWallType(0, t.getHorizontalWallVal() & 0xff)) return false;
			if ((dir == 2 || dir == 3) && !containsWallType(0, t.getVerticalWallVal() & 0xff)) return false;
		}
		if (((b & 32) != 0 || (b & 16) != 0) && !containsWallType(1, t.getDiagWallVal())) return false;

		if ((b & 64) != 0) return false;

		GameObject obj = World.getWorld().getTile(x, y).getGameObject();
		if (obj != null) {
			DoorDef d = obj.getDoorDef();
			if (d != null && ((face == 1 || face == 4) && obj.getDirection() == 0) || ((face == 2 || face == 8) && obj.getDirection() == 1)) {
				if (d.getCommand1().equalsIgnoreCase("open") || d.getCommand2().equalsIgnoreCase("open")) return false;
			}
		}
		return true;
	}

}
