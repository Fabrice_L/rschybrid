package org.rscdaemon.server.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.TreeMap;

import org.rscdaemon.server.ClientUpdater;
import org.rscdaemon.server.DelayedEventHandler;
import org.rscdaemon.server.GUI;
import org.rscdaemon.server.Server;
import org.rscdaemon.server.entityhandling.locs.GameObjectLoc;
import org.rscdaemon.server.entityhandling.locs.NPCLoc;
import org.rscdaemon.server.event.DelayedEvent;
import org.rscdaemon.server.event.SingleEvent;
import org.rscdaemon.server.io.WorldLoader;
import org.rscdaemon.server.model.item.Item;
import org.rscdaemon.server.model.item.Shop;
import org.rscdaemon.server.model.landscape.ActiveTile;
import org.rscdaemon.server.model.landscape.MutableTileValue;
import org.rscdaemon.server.model.landscape.TileValue;
import org.rscdaemon.server.model.npc.Npc;
import org.rscdaemon.server.model.player.Player;
import org.rscdaemon.server.npchandler.NpcHandler;
import org.rscdaemon.server.npchandler.NpcHandlerDef;
import org.rscdaemon.server.states.CombatState;
import org.rscdaemon.server.util.DataConversions;
import org.rscdaemon.server.util.EntityList;
import org.rscdaemon.server.util.Logger;
import org.rscdaemon.server.util.PersistenceManager;

public final class World {
	/**
	 * World instance
	 */
	private static World worldInstance;
	/**
	 * The maximum width of the map
	 */
	public static final int MAX_WIDTH = 944;
	/**
	 * The maximum height of the map (944 squares per level)
	 */
	public static final int MAX_HEIGHT = 3776;
	/**
	 * The tiles the map is made up of
	 */
	public ActiveTile[][] tiles = new ActiveTile[MAX_WIDTH][MAX_HEIGHT];
	/**
	 * Data about the tiles, are they walkable etc
	 */
	private TileValue[][] tileType = new TileValue[MAX_WIDTH][MAX_HEIGHT];
	/**
	 * A list of all players on the server
	 */
	private EntityList<Player> players = new EntityList<Player>(2000);
	/**
	 * A list of all npcs on the server
	 */
	private EntityList<Npc> npcs = new EntityList<Npc>(4000);
	/**
	 * The client updater instance
	 */
	private ClientUpdater clientUpdater;
	/**
	 * The delayedeventhandler instance
	 */
	private DelayedEventHandler delayedEventHandler;
	/**
	 * The server instance
	 */
	private Server server;
	/**
	 * A list of all shops on the server
	 */
	private List<Shop> shops = new ArrayList<Shop>();
	/**
	 * The mapping of npc IDs to their handler
	 */
	private TreeMap<Integer, NpcHandler> npcHandlers = new TreeMap<Integer, NpcHandler>();

	/**
	 * returns the only instance of this world, if there
	 * is not already one, makes it and loads everything
	 */
	public static synchronized World getWorld() {
		if (worldInstance == null) {
			worldInstance = new World();
			try {
				WorldLoader wl = new WorldLoader();
				wl.loadWorld(worldInstance);
				worldInstance.loadNpcHandlers();
			}
			catch(Exception e) {
				Logger.error(e);
			}	
		}
		return worldInstance;
	}

	/**
	 * returns the associated npc handler
	 */
	public NpcHandler getNpcHandler(int npcID) {
		return npcHandlers.get(npcID);
	}

	public void sendToAll(String s) {
		World world = World.getWorld();
		for(Player p : world.getPlayers()) {
			p.getActionSender().sendMessage(s);
		}
	}
	
	public void deletePlayer(String player) {
		if(GUI.isOnline(player)) {
			this.banPlayer(player);
		}
		File f = new File("players/" + player + ".cfg");
		if(f.exists()) {
			f.delete();
		}
		GUI.populateWorldList();
	}

	public void mutePlayer(String player) {
		Player p = this.getPlayer(DataConversions.usernameToHash(player));
		if(p != null) {
			p.rank = 5;
		} else {
			GUI.writeValue(player, "rank", "5");
		}
	}
	public void kickPlayer(String p) {
		try {
		Player player = this.getPlayer(DataConversions.usernameToHash(p));
		String play = player.getUsername().replaceAll(" ","_");
		File f = new File("players/" + play.toLowerCase() + ".cfg");
		Properties pr = new Properties();

		FileInputStream fis = new FileInputStream(f);
		pr.load(fis);
		fis.close();

		FileOutputStream fos = new FileOutputStream(f);
		pr.setProperty("loggedin", "false");
		pr.store(fos, "Character Data.");
		fos.close();

		for(Player pla : this.getPlayers()) {
			if(pla.isFriendsWith(player.getUsername())) {
				pla.getActionSender().sendFriendUpdate(player.getUsernameHash(), 0);
			}
		}
		player.save();
		player.destroy(true);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void unbanPlayer(String player) {
		
		GUI.writeValue(player, "rank", "0");
	}
	public void unMutePlayer(String p) {
		Player player = this.getPlayer(DataConversions.usernameToHash(p));
		if(player != null) {
			player.rank = 0;
		} else {
			GUI.writeValue(p, "rank", "5");
		}
		
	}
	public void banPlayer(String player) {
		player= player.replaceAll(" ","_");
		if(GUI.isOnline(player)) {
			Player p = this.getPlayer(DataConversions.usernameToHash(player));
			p.rank = 6;
			kickPlayer(player);
		} else {
			GUI.writeValue(player, "rank", "6");
		}
		
	}



	/**
	 * Loads the npc handling classes
	 */
	private void loadNpcHandlers() {
		NpcHandlerDef[] handlerDefs = (NpcHandlerDef[])PersistenceManager.load("NpcHandlers.xml");
		for(NpcHandlerDef handlerDef : handlerDefs) {
			try {
				String className = handlerDef.getClassName();
				Class<?> c = Class.forName(className);
				if (c != null) {
					NpcHandler handler = (NpcHandler)c.newInstance();
					for(int npcID : handlerDef.getAssociatedNpcs()) {
						npcHandlers.put(npcID, handler);
					}
				}
			}
			catch (Exception e) {
				Logger.error(e);
			}
		}
	}

	/**
	 * Inserts a new shop into the world
	 */
	public void registerShop(final Shop shop) {
		shop.setEquilibrium();
		shops.add(shop);
	}

	public List<Shop> getShops() {
		return shops;
	}

	/**
	 * Gets a list of all shops
	 */
	public Shop getShop(Point location) {
		for(Shop shop : shops) {
			if(shop.withinShop(location)) {
				return shop;
			}
		}
		return null;
	}

	/**
	 * Sets the instance of the server
	 */
	public void setServer(Server server) {
		this.server = server;
	}

	/**
	 * Gets the server instance
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * Sets the ClientUpdater instance
	 */
	public void setClientUpdater(ClientUpdater clientUpdater) {
		this.clientUpdater = clientUpdater;
	}

	/**
	 * Sets the DelayedEventHandler instance
	 */
	public void setDelayedEventHandler(DelayedEventHandler delayedEventHandler) {
		this.delayedEventHandler = delayedEventHandler;
	}

	/**
	 * Gets the ClientUpdater instance
	 */
	public ClientUpdater getClientUpdater() {
		return clientUpdater;
	}

	/**
	 * Gets the DelayedEventHandler instance
	 */
	public DelayedEventHandler getDelayedEventHandler() {
		return delayedEventHandler;
	}

	/**
	 * adds or removes the given entity from the relivant tiles
	 */
	public void setLocation(Entity entity, Point oldPoint, Point newPoint) {
		ActiveTile t;
		if (oldPoint != null) {
			t = getTile(oldPoint);
			t.remove(entity);
		}
		if (newPoint != null) {
			t = getTile(newPoint);
			t.add(entity);
		}
	}

	/**
	 * Are the given coords within the world boundaries
	 */
	public boolean withinWorld(int x, int y) {
		return x >= 0 && x < MAX_WIDTH && y >= 0 && y < MAX_HEIGHT;
	}

	/**
	 * Gets the tile value as point x, y
	 */
	public TileValue getTileValue(int x, int y) {
		if (!withinWorld(x, y)) {
			return null;
		}
		TileValue t = tileType[x][y];
		if (t == null) {
			t = TileValue.create(0, new byte[6]);
			tileType[x][y] = t;
		}
		return t;
	}
	
	/**
	 * Gets the active tile at point x, y
	 */
	public ActiveTile getTile(int x, int y) {
		if(!withinWorld(x, y)) {
			return null;
		}
		ActiveTile t = tiles[x][y];
		if (t == null) {
			t = new ActiveTile(x, y);
			tiles[x][y] = t;
		}
		return t;
	}

	/**
	 * Gets the tile at a point
	 */
	public ActiveTile getTile(Point p) {
		return getTile(p.getX(), p.getY());
	}

	/**
	 * Adds a DelayedEvent that will spawn a GameObject
	 */
	public void delayedSpawnObject(final GameObjectLoc loc, final int respawnTime) {
		delayedEventHandler.add(new SingleEvent(null, respawnTime) {
			public void action() {
				registerGameObject(new GameObject(loc));
			}
		});
	}

	/**
	 * Adds a DelayedEvent that will remove a GameObject
	 */
	public void delayedRemoveObject(final GameObject object, final int delay) {
		delayedEventHandler.add(new SingleEvent(null, delay) {
			public void action() {
				ActiveTile tile = getTile(object.getLocation());
				if(tile.hasGameObject() && tile.getGameObject().equals(object)) {
					unregisterGameObject(object);
				}
			}
		});
	}

	/**
	 * Registers a player with the world and informs other players on their login
	 */
	public void registerPlayer(Player p) {
		p.setInitialized();
		players.add(p);
	}

	/**
	 * Registers an npc with the world
	 */
	public void registerNpc(Npc n) {
		NPCLoc npc = n.getLoc();
		if(npc.startX < npc.minX || npc.startX > npc.maxX || npc.startY < npc.minY || npc.startY > npc.maxY || (getTileValue(npc.startX, npc.startY).mapValue & 64) != 0) {
			System.out.println("Fucked Npc: <id>" + npc.id + "</id><startX>" + npc.startX + "</startX><startY>" + npc.startY + "</startY>");
		}
		npcs.add(n);
	}

	/**
	 * Checks if the given player is logged in
	 */
	public boolean isLoggedIn(long usernameHash) {
		Player friend = getPlayer(usernameHash);
		if(friend != null) {
			return friend.loggedIn();
		}
		return false;
	}

	/**
	 * Registers an object with the world
	 */
	public void registerGameObject(GameObject o) {
		switch(o.getType()) {
		case 0:
			registerObject(o);
			break;
		case 1:
			registerDoor(o);
			break;
		}
	}

	/**
	 * Updates the map to include a new object
	 */
	public void registerObject(GameObject o) {
		if (o.getGameObjectDef().getType() != 1
				&& o.getGameObjectDef().getType() != 2) {
			return;
		}
		int dir = o.getDirection();
		int width, height;
		if (dir == 0 || dir == 4) {
			width = o.getGameObjectDef().getWidth();
			height = o.getGameObjectDef().getHeight();
		} else {
			height = o.getGameObjectDef().getWidth();
			width = o.getGameObjectDef().getHeight();
		}
		for (int x = o.getX(); x < o.getX() + width; x++) {
			for (int y = o.getY(); y < o.getY() + height; y++) {
				MutableTileValue t = new MutableTileValue(getTileValue(x, y));
				if (o.getGameObjectDef().getType() == 1) {
					t.objectValue |= 0x40;
				} else if (dir == 0) {
					t.objectValue |= 2;
					MutableTileValue t1 = new MutableTileValue(getTileValue(
							x - 1, y));
					t1.objectValue |= 8;
					setTileValue(x - 1, y, t1.toTileValue());
				} else if (dir == 2) {
					t.objectValue |= 4;
					MutableTileValue t1 = new MutableTileValue(getTileValue(x,
							y + 1));
					t1.objectValue |= 1;
					setTileValue(x, y + 1, t1.toTileValue());
				} else if (dir == 4) {
					t.objectValue |= 8;
					MutableTileValue t1 = new MutableTileValue(getTileValue(
							x + 1, y));
					t1.objectValue |= 2;
					setTileValue(x + 1, y, t1.toTileValue());
				} else if (dir == 6) {
					t.objectValue |= 1;
					MutableTileValue t1 = new MutableTileValue(getTileValue(x,
							y - 1));
					t1.objectValue |= 4;
					setTileValue(x, y - 1, t1.toTileValue());
				}
				setTileValue(x, y, t.toTileValue());
			}
		}

	}

	/**
	 * Updates the map to include a new door
	 */
	public void registerDoor(GameObject o) {
		if (o.getDoorDef().getDoorType() != 1) {
			return;
		}
		int dir = o.getDirection();
		int x = o.getX(), y = o.getY();
		MutableTileValue t = new MutableTileValue(getTileValue(x, y));
		if (dir == 0) {
			t.objectValue |= 1;
			MutableTileValue t1 = new MutableTileValue(getTileValue(x, y - 1));
			t1.objectValue |= 4;
			setTileValue(x, y - 1, t1.toTileValue());
		} else if (dir == 1) {
			t.objectValue |= 2;
			MutableTileValue t1 = new MutableTileValue(getTileValue(x - 1, y));
			t1.objectValue |= 8;
			setTileValue(x - 1, y, t1.toTileValue());
		} else if (dir == 2) {
			t.objectValue |= 0x10;
		} else if (dir == 3) {
			t.objectValue |= 0x20;
		}
		setTileValue(x, y, t.toTileValue());
	}
	
	/**
	 * Removes an object from the map
	 */
	public void unregisterObject(GameObject o) {
		if (o.getGameObjectDef().getType() != 1
				&& o.getGameObjectDef().getType() != 2) {
			return;
		}
		int dir = o.getDirection();
		int width, height;
		if (dir == 0 || dir == 4) {
			width = o.getGameObjectDef().getWidth();
			height = o.getGameObjectDef().getHeight();
		} else {
			height = o.getGameObjectDef().getWidth();
			width = o.getGameObjectDef().getHeight();
		}
		for (int x = o.getX(); x < o.getX() + width; x++) {
			for (int y = o.getY(); y < o.getY() + height; y++) {
				MutableTileValue t = new MutableTileValue(getTileValue(x, y));

				if (o.getGameObjectDef().getType() == 1) {
					t.objectValue &= 0xffbf;
				} else if (dir == 0) {
					t.objectValue &= 0xfffd;
					MutableTileValue t1 = new MutableTileValue(getTileValue(
							x - 1, y));
					t1.objectValue &= 65535 - 8;
					setTileValue(x - 1, y, t1.toTileValue());
				} else if (dir == 2) {
					t.objectValue &= 0xfffb;
					MutableTileValue t1 = new MutableTileValue(getTileValue(x,
							y + 1));
					t1.objectValue &= 65535 - 1;
					setTileValue(x, y + 1, t1.toTileValue());
				} else if (dir == 4) {
					t.objectValue &= 0xfff7;
					MutableTileValue t1 = new MutableTileValue(getTileValue(
							x + 1, y));
					t1.objectValue &= 65535 - 2;
					setTileValue(x + 1, y, t1.toTileValue());
				} else if (dir == 6) {
					t.objectValue &= 0xfffe;
					MutableTileValue t1 = new MutableTileValue(getTileValue(x,
							y - 1));
					t1.objectValue &= 65535 - 4;
					setTileValue(x, y - 1, t1.toTileValue());
				}
				setTileValue(x, y, t.toTileValue());
			}
		}
	}
	/**
	 * Removes a door from the map
	 */
	public void unregisterDoor(GameObject o) {
		if (o.getDoorDef().getDoorType() != 1) {
			return;
		}
		int dir = o.getDirection();
		int x = o.getX(), y = o.getY();
		MutableTileValue t = new MutableTileValue(getTileValue(x, y));

		if (dir == 0) {
			t.objectValue &= 0xfffe;
			MutableTileValue t1 = new MutableTileValue(getTileValue(x, y - 1));
			t1.objectValue &= 65535 - 4;
			setTileValue(x, y - 1, t1.toTileValue());
		} else if (dir == 1) {
			t.objectValue &= 0xfffd;
			MutableTileValue t1 = new MutableTileValue(getTileValue(x - 1, y));
			t1.objectValue &= 65535 - 8;
			setTileValue(x - 1, y, t1.toTileValue());
		} else if (dir == 2) {
			t.objectValue &= 0xffef;
		} else if (dir == 3) {
			t.objectValue &= 0xffdf;
		}
		setTileValue(x, y, t.toTileValue());
	}
	
	public void setTileValue(int x, int y, TileValue tileValue) {
		tileType[x][y] = tileValue;
	}

	/**
	 * Registers an item to be removed after 3 minutes
	 */
	public void registerItem(final Item i) {
		if(i.getLoc() == null) {
			delayedEventHandler.add(new DelayedEvent(null, 180000) {
				public void run() {
					ActiveTile tile = getTile(i.getLocation());
					if(tile.hasItem(i)) {
						unregisterItem(i);
					}
					running = false;
				}
			});
		}
	}

	/**
	 * Removes a player from the server and saves their account
	 */
	public void unregisterPlayer(Player p) {
		p.setLoggedIn(false);
		p.resetAll();
		p.save();
		Mob opponent = p.getOpponent();
		if(opponent != null) {
			p.resetCombat(CombatState.ERROR);
			opponent.resetCombat(CombatState.ERROR);
		}
		delayedEventHandler.removePlayersEvents(p);
		players.remove(p);
		setLocation(p, p.getLocation(), null);
	}

	/**
	 * Removes an npc from the server
	 */
	public void unregisterNpc(Npc n) {
		if(hasNpc(n)) {
			npcs.remove(n);
		}
		setLocation(n, n.getLocation(), null);
	}

	/**
	 * Removes an object from the server
	 */
	public void unregisterGameObject(GameObject o) {
		o.remove();
		setLocation(o, o.getLocation(), null);
		switch(o.getType()) {
		case 0:
			unregisterObject(o);
			break;
		case 1:
			unregisterDoor(o);
			break;
		}
	}

	/**
	 * Removes an item from the server
	 */
	public void unregisterItem(Item i) {
		i.remove();
		setLocation(i, i.getLocation(), null);
	}

	/**
	 * Gets the list of players on the server
	 */
	public EntityList<Player> getPlayers() {
		return players;
	}

	/**
	 * Gets the list of npcs on the server
	 */
	public EntityList<Npc> getNpcs() {
		return npcs;
	}

	/**
	 * Counts how many players are currently connected
	 */
	public int countPlayers() {
		return players.size();
	}

	/**
	 * Counts how many npcs are currently here
	 */
	public int countNpcs() {
		return npcs.size();
	}

	/**
	 * Checks if the given npc is on the server
	 */
	public boolean hasNpc(Npc n) {
		return npcs.contains(n);
	}

	/**
	 * Checks if the given player is on the server
	 */
	public boolean hasPlayer(Player p) {
		return players.contains(p);
	}

	/**
	 * Gets a player by their username hash
	 */
	public Player getPlayer(long usernameHash) {
		for(Player p : players) {
			if(p.getUsernameHash() == usernameHash) {
				return p;
			}
		}
		return null;
	}
	public Player getPlayer(String username)
	{
		return getPlayer(DataConversions.usernameToHash(username));
	}

	/**
	 * Gets an Npc by their server index
	 */
	public Npc getNpc(int idx) {
		return npcs.get(idx);
	}

	/**
	 *  Gets an npc by their coords and id]
	 */
	public Npc getNpc(int id, int minX, int maxX, int minY, int maxY) {
		for(Npc npc : npcs) {
			if(npc.getID() == id && npc.getX() >= minX && npc.getX() <= maxX && npc.getY() >= minY && npc.getY() <= maxY) {
				return npc;
			}
		}
		return null;
	}

	/**
	 * Gets a Player by their server index
	 */
	public Player getPlayer(int idx) {
		return players.get(idx);
	}
}
