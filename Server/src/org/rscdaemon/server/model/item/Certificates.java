package org.rscdaemon.server.model.item;

public enum Certificates {
	ORES(new int[][]{
		{517, 151}, //Iron
		{518, 155}, //Coal
		{519, 153}, //Mithril
		{1295, 154}, //Adamantite
		{521, 152}, //Gold
		{520, 383} //Silver
	}),
	BARS(new int[][]{
		{528, 170}, //Iron
		{529, 171}, //Steel
		{530, 173}, //Mithril
		{1296, 174}, //Adamantite
		{532, 172}, //Gold
		{531, 384} //Silver
	}),
	RAW_FISH(new int[][]{
		{1305, 358}, //Trout
		{1307, 356}, //Salmon
		{534, 372}, //Lobster
		{536, 369}, //SwordFish
		{1292, 366}, //Tuna
		{631, 545}, //Shark
		{629, 554} //Bass
	}),
	COOKED_FISH(new int[][]{
		{1306, 359}, //Trout
		{1308, 357}, //Salmon
		{533, 373}, //Lobster
		{535, 370}, //SwordFish
		{1291, 367}, //Tuna
		{630, 546}, //Shark
		{628, 555} //Bass
	}),
	POTION(new int[][]{
		{1302, 474}, //Attack Potion
		{1304, 222}, //Strength Potion
		{1303, 480}, //Defense Potion
		{1273, 486}, //Super Attack Potion
		{1274, 495}, //Super Defense Potion
		{1275, 492}, //Super Strength Potion
		{1272, 483}, //Prayer Potion
		{1301, 498} //Range Potion
	}),
	BONES(new int[][]{
		{1300, 413}, //Big Bones
		{1270, 814} //Dragon Bones
	}),
	LOGS(new int[][]{
		{1290, 632}, //Oak Logs
		{713, 633}, //Willow Logs
		{712, 634}, //Maple Logs
		{711, 635}, //Yew Logs
		{1310, 636} //Magic Logs
	}),
	FLAX(new int[][]{
		{1293, 675}, //Flax
		{1294, 676} //Bow Strings
	}),
	HERBLORE(new int[][]{
		{1297, 465}, //Empty Vial
		{1298, 464}, //Full Vial
		{1299, 270}, //Eye Of Newt
		{1271, 220}, //Limpwurt Root
		{1311, 469}, //Snape Grass
		{1312, 471}, //White Berries
		{1313, 219} //Red Spider Eggs
	});
	
	private int[][] items; // {{CertificateId, itemID}}
	
	Certificates(int[][] items) {
		this.items = items;
	}
	
	public static int getCertId(int itemId) {
		for (Certificates certs: Certificates.values()) {
			for (int i = 0; i < certs.items.length; i++) {
				if (certs.items[i][1] == itemId)
					return certs.items[i][0];
			}
		}
		return -1;
	}
	
	public static int getItemId(int certId) {
		for (Certificates certs: Certificates.values()) {
			for (int i = 0; i < certs.items.length; i++) {
				if (certs.items[i][0] == certId)
					return certs.items[i][1];
			}
		}
		return -1;
	}
}
